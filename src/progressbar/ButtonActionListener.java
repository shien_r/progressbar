package progressbar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JProgressBar;

public class ButtonActionListener implements ActionListener {
	private ProgressWorker progressWorker;
	private JProgressBar jProgressBar;
	private final int MAX = 10_000_000;
	
	ButtonActionListener (ProgressWorker _progressWorker) {
		progressWorker = _progressWorker;
	}
	
	ButtonActionListener (JProgressBar _jProgressBar) {
		jProgressBar = _jProgressBar;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (progressWorker != null) {
			progressWorker.execute();
		} else {
			for (int i=0; i < MAX; i++) {
				jProgressBar.setValue(i);
			}
		}
		
	}

}
