package progressbar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

public class ProgressBar extends JFrame implements Runnable {
	private final boolean MULTI_THREAD_FLAG = true;
	ProgressBar () {
		this.setSize(500, 500);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("Progress Bar");
		this.setLayout(new FlowLayout());
		JProgressBar jProgressBar = new JProgressBar();
		jProgressBar.setPreferredSize(new Dimension(100,40));
		jProgressBar.setStringPainted(true);
		jProgressBar.setForeground(Color.GREEN);
		jProgressBar.setBackground(Color.ORANGE);
		JButton jButton = new JButton("press start");
		if (MULTI_THREAD_FLAG) {
			ProgressWorker progressWorker = new ProgressWorker(jProgressBar, jButton);
			jButton.addActionListener(new ButtonActionListener(progressWorker));
		} else {
			jButton.addActionListener(new ButtonActionListener(jProgressBar));
		}
		JPanel pPanel = new JPanel();
		pPanel.add(jProgressBar);
		add(jButton);
		add(pPanel);
	} 
	 
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new ProgressBar());
	}

	@Override
	public void run() {
		this.setVisible(true);		
	}

}
