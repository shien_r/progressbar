package progressbar;

import javax.swing.JButton;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

public class ProgressWorker extends SwingWorker<Object, Object> {
	private final int MAX = 10_000_000;
	private JProgressBar jProgressBar;
	private JButton jButton;
	
	ProgressWorker (JProgressBar _jProgressBar, JButton _jButton) {
		jProgressBar = _jProgressBar;
		jProgressBar.setMaximum(MAX);
		jProgressBar.setMinimum(0);
		jButton = _jButton;
	}
	
	@Override
	protected Object doInBackground() throws Exception {
		for (int i=0; i < MAX; i++) {
			jProgressBar.setValue(i);
		}
		return null;
	}
	
	@Override
    protected void done() {
        jButton.setText("おわた");
        jButton.setEnabled(true);
    }

}
